.. include:: replace.txt

DSDV Routing
------------

Destination-Sequenced Distance Vector (DSDV) routing protocol is a pro-active, table-driven routing protocol
for MANETs developed by Charles E. Perkins and Pravin Bhagwat in 1994. It uses the hop count as metric in route
selection.

This model was developed by 
`the ResiliNets research group <http://www.ittc.ku.edu/resilinets>`_
at the University of Kansas.  A paper on this model exists at
`this URL <https://wiki.ittc.ku.edu/resilinets/ResiliNets_Publications#.E2.80.9CDestination-Sequenced_Distance_Vector_.28DSDV.29_Routing_Protocol_Implementation_in_ns-3.E2.80.9D>`_. 

DSDV Routing Overview
*********************

The DSDV routing protocol ``ns3::dsdv::RoutingProtocol`` is implemented by extending from the base
abstract class ``ns3::Ipv4Routing Protocol``.
The class ``ns3::dsdv::DsdvHeader`` implements the message header and it is extended from  ``ns3::header``. The DSDV message header is 32 bits wide
with a total size of 12 bytes. The fields are node's IP address, the number of hops and sequence number.
The header implementation can be found in dsdv-packet.cc file.

DSDV Routing Table: 
The routing table is implemented by ``ns3::dsdv::RoutingTableEntry`` to store the updates of a node and ``ns3::dsdv:: RoutingTable``  to  store
all  these  entries  in  a table.Every node will maintain a table listing all the other nodes it has known either directly
or through some neighbors. Every node has a single entry in the routing table. The entry will have information
about the node's IP address, last known sequence number and the hop count to reach that node. Along with these
details the table also keeps track of the nexthop neighbor to reach the destination node, the timestamp of the last
update received for that node.The RoutingTable class has methods to add,delete,update,look up, and print entries.
DSDV maintains two routing tables: a permanent routing table and an advertising routing table.
These tables store  the permanent stable routes and the recently received routes respectively. The recently received
routes might be unstable so the stability of routes are identified using Settling Time.This Settling time is the time
a node waits before advertising a route so that the route becomes stable.

The DSDV update message consists of three fields, Destination Address, Sequence Number and Hop Count.

Each node uses 2 mechanisms to send out the DSDV updates. They are,

1. Periodic Updates
    Periodic updates are sent out after every m_periodicUpdateInterval(default:15s). In this update the node broadcasts
    out its entire routing table to other nodes in its broadcast range.
2. Trigger Updates
    Trigger Updates are small updates in-between the periodic updates. These updates are sent out whenever a node
    receives a DSDV packet that caused a change in its routing table. The original paper did not clearly mention
    when for what change in the table should a DSDV update be sent out. The current implementation sends out an update
    irrespective of the change in the routing table.

The updates are accepted based on the metric for a particular node. The first factor determining the acceptance of
an update is the sequence number. Whenever a node broadcasts a periodic update the sequence number is incremented by 2. 
So the sequence number of a normal update is an even number. If the sequence number is odd then the node will 
remove the corresponding entry from the routing table indicating an expired route. It has to accept the update if the sequence number of the update message is higher
irrespective of the metric. If the update with same sequence number is received, then the update with least metric
(hopCount) is given precedence. 

In highly mobile scenarios, there is a high chance of route fluctuations, thus we have the concept of weighted
settling time where an update with change in metric will not be advertised to neighbors. The node waits for
the settling time to make sure that it did not receive the update from its old neighbor before sending out
that update.

For fairer comparison between disruption-tolerant networks, domain-specific  MANET's  and for packets with 
 no destination the buffering mechanism was implemented though not present in the original paper. The packet buffering is impemented using 
declared the ``ns3::dsdv::QueueEntry`` class to store a packet and  ``ns3::dsdv::RequestQueue`` to store all the queued entries.
The default is set to buffer up to 5 packets per destination.

Useful parameters
=================

:: 

   +------------------------- +------------------------------------+-------------+
   | Parameter                | Description                        | Default     |
   +==========================+====================================+=============+
   | PeriodicUpdateInterval   | Periodic interval between exchange | Seconds(15) |
   |                          | of full routing tables among nodes |             |
   +------------------------- +------------------------------------+-------------+
   | SettlingTime             | Minimum time an update is to be    | Seconds(5)  |
   |                          | stored in adv table before sending |             |
   |                          |out in case of change in metric     |             |
   +------------------------- +------------------------------------+-------------+
   | MaxQueueLen              | Maximum number of packets that we  | 500         |
   |                          | allow a routing protocol to buffer |             |
   +------------------------- +------------------------------------+-------------+
   | MaxQueuedPacketsPerDst   | Maximum number of packets that we  | 5           |
   |                          | allow per destination to buffer    |             |
   +------------------------- +------------------------------------+-------------+
   | MaxQueueTime             | Maximum time packets can be queued | Seconds(30) |
   |                          |                                    |             |
   +------------------------- +------------------------------------+-------------+
   | EnableBuffering          | Enables buffering of data packets  | True        |
   |                          | if no route to destination is      |             |
   |                          | available                          |             |
   +------------------------- +------------------------------------+-------------+
   | EnableWST                | Enables Weighted Settling Time for | True        |
   |                          | the updates before advertising     |             |
   +------------------------- +------------------------------------+-------------+
   | Holdtimes                | Times the forwarding Interval to   | 3           |
   |                          | purge the route                    |             |
   +------------------------- +------------------------------------+-------------+
   | WeightedFactor           | WeightedFactor for the settling    | 0.875       |
   |                          | time if Weighted Settling Time is  |             |
   |                          | enabled                            |             |
   +------------------------- +------------------------------------+-------------+
   | EnableRouteAggregation   | Enables aggregation of DSDV updates| False       |
   |                          | over a period of time              |             |
   +--------------------------+------------------------------------+-------------+
   | RouteAggregationTime     | Time to aggregate updates before   | Seconds(1)  |
   |                          | sending them out                   |             |
   +--------------------------+------------------------------------+-------------+

Helper
******

To have a node run DSR, the easiest way would be to use the DsdvHelper in your
simulation script. For instance:

.. sourcecode:: cpp

  DsdvHelper dsdv;
  dsdv.Set ("PeriodicUpdateInterval", TimeValue (Seconds (m_periodicUpdateInterval)));
  dsdv.Set ("SettlingTime", TimeValue (Seconds (m_settlingTime)));
  InternetStackHelper stack;
  stack.SetRoutingHelper (dsdv); // has effect on the next Install ()
  stack.Install (nodes);

The example scripts inside ``src/dsdv/examples/`` demonstrate the use of DSDV based nodes in different scenarios.
The helper source can be found inside ``src/dsdv/helper/dsdv-helper.{h,cc}``

Examples
********

The example can be found in ``src/dsdv/examples/``

DSDV is also built in the routing comparison case in ``examples/routing/``:

* ``manet-routing-compare.cc`` is a comparison case with built in MANET routing protocols and can generate its own results.

Validation
**********

This model has been tested as follows:

* Unit tests have been written to verify the internals of DSDV. This can be found in ``src/dsdv/test/dsdv-testcase.cc``. These tests verify whether the methods inside DSDV module work correctly.
* manet-routing-compare.cc has been used to compare DSDV with three other routing protocols.

References
**********

Link to the Paper: http://portal.acm.org/citation.cfm?doid=190314.190336

DSDV implementation in ns-3: http://www.ittc.ku.edu/resilinets/papers/Narra-Cheng-Cetinkaya-Rohrer-Sterbenz-2011.pdf
